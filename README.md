# TP3 : Sujet de maintenance du Thermo-geeko-stat

Nous avons vu en cours que la maintenance de logiciel est une activité importante notamment par les coûts.

Dans ce TP vous allez toucher du doigt les difficultés inhérentes à cette tâche, et surtout les difficultés liés à la compréhension du code de quelqu'un d'autre.
(C'est une chose que vous devrez faire souvent dans la vie professionnelle.)

## Maintenance du thermo-geeko-stat

Dans le TP précédant vous avez créé un Thermo-geeko-stat qui permet de controler une température dans plusieurs échelles différentes.
Re-voici l’exemple final d’affichage qui était demandé :

![Exemple des vues du thermo-geeko-stat](documentation/multipleViewsAndScales.png)

On vous demande d'implémenter des extensions au programme initial.
Pour être dans des conditions de travail plus réaliste, vous ferez ces modifications sur le TP de quelqu'un d'autre, et il vous faudra donc vous plonger dans un code que vous ne connaissez pas bien (même si ici la structure du projet était fortement contrainte).

Vous pouvez reprendre le TP d'un collègue qui avait fini, ou bien utilisé une solution au [TP proposé ici](https://gitlab.univ-lille.fr/nicolas.anquetil/solution-tp-mvc-thermostat.git)

Avec un programme bien fait, les extensions ci-dessous devraient se faire assez simplement.
Mais quelque soit le projet que vous reprendrez, ce plonger dans le code de quelqu'un d'autre est toujours difficile.

Jouez le jeu, si vous utilisez un TP d'un collègue, ne lui demandez pas de vous aider.

## Extensions demandées

1. Dans la fenêtre avec un TextField, si la température est au-delà de 25⁰C (converti dans l’échelle de la fenêtre), le champ texte devient rouge et en deça de -5⁰C le champ texte devient bleu
      <votre TextField>.setStyle("-fx-control-inner-background:blue;")
2. Rajoutez un *timer* simple qui 5 secondes après tout changement de température ramène celle-ci à 18⁰C (converti dans l’échelle de la fenêtre)
3. Faire une nouvelle fenêtre qui pour chaque échelle de température offre deux boutons : « FenêtreTexte » et « FenêtreSlider ». Cliquer sur un des deux boutons créera une fenêtre comme dans l'exemple ci-dessus dans l’échelle correspondante.
